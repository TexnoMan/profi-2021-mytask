FROM registry.gitlab.com/roboforces-itmo/open-tasks/iprofi2021/profi2021_bachelor_docker:ubuntu-develop

RUN apt-get update && apt-get upgrade -y
COPY . /ros_ws/src/solution

WORKDIR /ros_ws

RUN apt-get install python-is-python3

RUN /bin/bash -c "catkin build"

ENTRYPOINT ["/bin/bash","-ci","/ros_ws/src/solution/entrypoint.bash"]
